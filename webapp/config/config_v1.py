
'''
	Version 1 Configuration
'''

# app url prefix (no trailing slash)
DEV = False
TEMPLATES_AUTO_RELOAD = True
PREFIX_V1 = '/api'
LOGCONFIG = 'webapp/config/config_logging.json'

RABBIT_HOST = "127.0.0.1"
RABBIT_USER = "user"
RABBIT_PASS = "user123"
RABBIT_MQTT_PORT = 1883
RABBIT_MQTT_WEB_PORT = 15675

SERVER_MAIN_FPS = 30
SERVER_PUBLISH_FPS = 5
#SERVER_REZ = (3280, 2464)
SERVER_REZ = (1640, 1242)
