#!/bin/sh

cd /deploy/drn_picam_release

sudo systemctl stop mycircus

git pull

mkdir log

sudo cp rc.local /etc/rc.local

sudo cp circus/mycircus.service /lib/systemd/system/mycircus.service
sudo systemctl enable mycircus

sudo rm /etc/nginx/sites-enabled/*
sudo cp nginx/site /etc/nginx/sites-available
sudo ln -s /etc/nginx/sites-available/site /etc/nginx/sites-enabled/site

#pip install circus bjoern chaussette flask flask_restful paho-mqtt
sudo pip install circus bjoern chaussette flask flask_restful blinker paho-mqtt picamera

sudo systemctl reload nginx
sudo systemctl start mycircus
